<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function comfortCategory() {
        return $this->belongsTo(ComfortCategory::class);
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class);
    }
}
