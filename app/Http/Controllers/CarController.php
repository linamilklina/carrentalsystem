<?php

namespace App\Http\Controllers;

use App\Http\Requests\CarRequest;
use App\Models\Booking;
use App\Models\Car;
use App\Models\ComfortCategory;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class CarController extends Controller
{
    public function getAvailableCars(CarRequest $request)
    {
        try {
            $validator = Validator::make($request->all(), $request->rules(), $request->messages());

            if ($validator->fails()) {
                $firstError = $validator->errors()->first();
                return response()->json(['error' => $firstError], 422);
            }

            $user = User::find($request->id);
            if (!$user) {
                return response()->json(['error' => 'Пользователь не найден'], 404);
            }
            $position = $user->position;
            if (!$position) {
                return response()->json(['error' => 'Должность не определена'], 404);
            }

            $allowedCategories = json_decode($position->allowed_categories);
            $allowedCategoriesIds = ComfortCategory::whereIn('name', $allowedCategories)
                ->pluck('id')
                ->all();

            $carIds = Booking::where(function ($query) use ($request) {
                $query->where('start_time', '<', $request->end_time)
                    ->where('end_time', '>', $request->start_time);
            })->pluck('car_id');

            $cars = Car::whereNotIn('id', $carIds)->get();

            if ($request->has('model')) {
                $cars = $cars->where('model', $request->model);
            }

            if ($request->has('comfort_category')) {
                $cars = $cars->filter(function ($car) use ($request) {
                    return $car->comfortCategory->name === $request->comfort_category;
                });
            }

            $availableCars = $cars->whereIn('comfort_category_id', $allowedCategoriesIds);

            return response()->json($availableCars);
        } catch (\Exception $e) {
            Log::error('Ошибка при получении доступных автомобилей: ' . $e->getMessage());
            return response()->json(['error' => 'Ошибка при получении доступных автомобилей'], 500);
        }
    }
}
