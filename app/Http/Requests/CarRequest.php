<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'id' => 'required|exists:users,id',
            'start_time' => 'required|date',
            'end_time' => 'required|date|after:start_time',
            'model' => 'string',
            'comfort_category' => 'string',
        ];
    }

    public function messages()
    {
        return [
            'id.required' => 'Идентификатор пользователя обязателен.',
            'id.exists' => 'Выбранный идентификатор пользователя недействителен.',
            'start_time.required' => 'Время начала обязательно.',
            'start_time.date' => 'Время начала должно быть действительной датой.',
            'end_time.required' => 'Время окончания обязательно.',
            'end_time.date' => 'Время окончания должно быть действительной датой.',
            'end_time.after' => 'Время окончания должно быть датой после времени начала.',
            'model.string' => 'Модель должна быть строкой.',
            'comfort_category.string' => 'Категория комфорта должна быть строкой.',
        ];
    }
}
