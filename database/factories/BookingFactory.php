<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BookingFactory extends Factory
{
    public function definition(): array
    {
        $startTime = fake()->dateTimeBetween('today', 'tomorrow');
        $endTime = fake()->dateTimeBetween('today', 'tomorrow');

        while ($startTime >= $endTime) {
            $startTime = fake()->dateTimeBetween('today', 'tomorrow');
            $endTime = fake()->dateTimeBetween('today', 'tomorrow');
        }

        return [
            'car_id' => rand(1, 5),
            'start_time' => $startTime,
            'end_time' => $endTime,
        ];
    }
}
