<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    public function definition(): array
    {
        return [
            'model' => fake()->word(),
            'comfort_category_id' => rand(1, 4),
            'driver_id' => rand(1, 5),
        ];
    }
}
