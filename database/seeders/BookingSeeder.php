<?php

namespace Database\Seeders;

use Database\Factories\BookingFactory;
use Illuminate\Database\Seeder;

class BookingSeeder extends Seeder
{
    public function run(): void
    {
        BookingFactory::new()->count(4)->create();
    }
}
