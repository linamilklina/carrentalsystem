<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PositionSeeder extends Seeder
{
    public function run(): void
    {
        $position = [
            [
                'name' => 'Менеджер по продажам',
                'allowed_categories' => '["первая", "вторая"]'
            ],
            [
                'name' => 'Финансовый аналитик',
                'allowed_categories' => '["вторая", "третья"]'
            ],
            [
                'name' => 'Инженер-программист',
                'allowed_categories' => '["первая", "третья"]'
            ],
            [
                'name' => 'HR-специалист',
                'allowed_categories' => '["вторая", "четвёртая"]'
            ],
            [
                'name' => 'Маркетолог',
                'allowed_categories' => '["первая", "четвёртая"]'
            ],
        ];

        foreach ($position as $value) {
            Position::create($value);
        }
    }
}
