<?php

namespace Database\Seeders;

use Database\Factories\DriverFactory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DriverSeeder extends Seeder
{
    public function run(): void
    {
        DriverFactory::new()->count(5)->create();
    }
}
