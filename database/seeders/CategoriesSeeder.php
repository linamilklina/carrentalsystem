<?php

namespace Database\Seeders;

use App\Models\ComfortCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    public function run(): void
    {
        $categories = [
            'первая',
            'вторая',
            'третья',
            'четвёртая',
        ];
        foreach ($categories as $value) {
            ComfortCategory::create(
                [
                    'name' => $value,
                ]
            );
        }
    }
}
