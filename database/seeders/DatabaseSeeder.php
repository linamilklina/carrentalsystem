<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
        $this->call([
            CategoriesSeeder::class,
            PositionSeeder::class,
            DriverSeeder::class,
            CarSeeder::class,
            BookingSeeder::class,
            UserSeeder::class,
        ]);
    }
}
