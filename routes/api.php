<?php

use App\Http\Controllers\CarController;
use Illuminate\Support\Facades\Route;



Route::get('/available-cars', [CarController::class, 'getAvailableCars']);

Route::get('/test', function () {
    return 'Hello, World!';
});
